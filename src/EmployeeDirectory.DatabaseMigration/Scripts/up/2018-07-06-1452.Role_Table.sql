﻿CREATE TABLE [dbo].[Role]
(
    [Id] UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT PK_Role PRIMARY KEY
		DEFAULT (NEWSEQUENTIALID()),
    [Name] NVARCHAR(255),
);
