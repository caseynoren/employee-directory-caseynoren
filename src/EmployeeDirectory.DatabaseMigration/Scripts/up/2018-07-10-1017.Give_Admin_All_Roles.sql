﻿DECLARE @employeeA UNIQUEIDENTIFIER;

SET @employeeA = NEWID();

INSERT INTO [dbo].[Employee] ([Id], [Email], [FirstName], [LastName], [JobTitle], [Office], [PhoneNumber], [HashedPassword]) 
	VALUES (@employeeA, N'casey@example.com', N'Casey', N'Koster', N'Intern', 1, N'555-555-5555', '$2a$10$fIQoNHZ02iNfdNty0BulRebQ7RV1ww79KVCyBhLeEQH9.IpipoxTu');

DECLARE @Id UNIQUEIDENTIFIER;
SET @Id = NEWID();

DECLARE @Id2 UNIQUEIDENTIFIER;
SET @Id2 = NEWID();

DECLARE @Id3 UNIQUEIDENTIFIER;
SET @Id3 = NEWID();

DECLARE @Admin UNIQUEIDENTIFIER;
SELECT @Admin = Id FROM [dbo].[Employee] WHERE Email = 'casey@example.com';

DECLARE @SystemAdmin UNIQUEIDENTIFIER;
SELECT @SystemAdmin = Id FROM dbo.Role WHERE Name = 'System Administrator'

DECLARE @Manager UNIQUEIDENTIFIER;
SELECT @Manager = Id FROM dbo.Role WHERE Name = 'Manager'

DECLARE @HR UNIQUEIDENTIFIER;
SELECT @HR = Id FROM dbo.Role WHERE Name = 'Human Resources'

INSERT INTO [dbo].[EmployeeRoles] VALUES (@Id, @Admin, @SystemAdmin);
INSERT INTO [dbo].[EmployeeRoles] VALUES (@Id2, @Admin, @Manager);
INSERT INTO [dbo].[EmployeeRoles] VALUES (@Id3, @Admin, @HR);
