﻿DECLARE @SystemAdmin UNIQUEIDENTIFIER;
SELECT @SystemAdmin = Id FROM dbo.Role WHERE Name = 'System Administrator'

DECLARE @Manager UNIQUEIDENTIFIER;
SELECT @Manager = Id FROM dbo.Role WHERE Name = 'Manager'

DECLARE @HR UNIQUEIDENTIFIER;
SELECT @HR = Id FROM dbo.Role WHERE Name = 'Human Resources'

DECLARE @Id UNIQUEIDENTIFIER;
SET @Id = NEWID();

DECLARE @Id2 UNIQUEIDENTIFIER;
SET @Id2 = NEWID();

DECLARE @Id3 UNIQUEIDENTIFIER;
SET @Id3 = NEWID();

DECLARE @Id4 UNIQUEIDENTIFIER;
SET @Id4 = NEWID();

DECLARE @Id5 UNIQUEIDENTIFIER;
SET @Id5 = NEWID();

INSERT INTO [dbo].[RolePermission] VALUES (@Id, @SystemAdmin, 4);
INSERT INTO [dbo].[RolePermission] VALUES (@Id2, @Manager, 3);
INSERT INTO [dbo].[RolePermission] VALUES (@Id3, @HR, 1);
INSERT INTO [dbo].[RolePermission] VALUES (@Id4, @HR, 2);
INSERT INTO [dbo].[RolePermission] VALUES (@Id5, @HR, 3);
