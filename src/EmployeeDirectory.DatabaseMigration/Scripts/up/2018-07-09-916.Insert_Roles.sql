﻿DECLARE @roleA UNIQUEIDENTIFIER;
DECLARE @roleB UNIQUEIDENTIFIER;
DECLARE @roleC UNIQUEIDENTIFIER;

SET @roleA = NEWID();
SET @roleB = NEWID();
SET @roleC = NEWID();

INSERT INTO [dbo].[Role] ([Id], [Name]) 
	VALUES (@roleA, N'Human Resources');

INSERT INTO [dbo].[Role] ([Id], [Name]) 
	VALUES (@roleB, N'Manager');

INSERT INTO [dbo].[Role] ([Id], [Name]) 
	VALUES (@roleC, N'System Administrator');
