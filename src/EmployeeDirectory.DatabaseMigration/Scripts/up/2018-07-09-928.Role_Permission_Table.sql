﻿CREATE TABLE [dbo].[RolePermission](
	Id UNIQUEIDENTIFIER NOT NULL,
	RoleId UNIQUEIDENTIFIER NOT NULL,
	Permission INT NOT NULL
	CONSTRAINT PK_RolePermission PRIMARY KEY (Id),
	CONSTRAINT FK_RolePermission_Id FOREIGN KEY (RoleId) REFERENCES Role(Id),
);
