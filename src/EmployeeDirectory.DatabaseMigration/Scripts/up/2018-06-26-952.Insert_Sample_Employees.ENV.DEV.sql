﻿DECLARE @customerA UNIQUEIDENTIFIER;
DECLARE @customerB UNIQUEIDENTIFIER;
DECLARE @customerC UNIQUEIDENTIFIER;

SET @customerA = NEWID();
SET @customerB = NEWID();
SET @customerC = NEWID();

INSERT INTO [dbo].[Employee] ([Id], [Email], [FirstName], [LastName], [JobTitle], [Office], [PhoneNumber]) 
	VALUES (@customerA, N'dustin@example.com', N'Dustin', N'Wells', N'President & CEO', 1, N'555-123-0001');

INSERT INTO [dbo].[Employee] ([Id], [Email], [FirstName], [LastName], [JobTitle], [Office], [PhoneNumber]) 
	VALUES (@customerB, N'vasudha@example.com', N'Vasudha', N'Prabhala', N'Vice President of Service Delivery', 1, N'555-123-0002');

INSERT INTO [dbo].[Employee] ([Id], [Email], [FirstName], [LastName], [JobTitle], [Office], [PhoneNumber]) 
	VALUES (@customerC, N'jimmy@example.com', N'Jimmy', N'Bogard', N'Chief Architect', 1, N'555-123-0003');
