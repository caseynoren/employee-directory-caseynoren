﻿ALTER TABLE [dbo].[Employee] ADD [HashedPassword] NVARCHAR(100) NULL;

GO

UPDATE [dbo].[Employee] SET [HashedPassword] = '$2a$10$fIQoNHZ02iNfdNty0BulRebQ7RV1ww79KVCyBhLeEQH9.IpipoxTu';

GO

ALTER TABLE [dbo].[Employee] ALTER COLUMN [HashedPassword] NVARCHAR(100) NOT NULL;

GO
