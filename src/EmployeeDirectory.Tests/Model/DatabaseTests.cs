﻿namespace EmployeeDirectory.Tests.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using EmployeeDirectory.Model;
    using static Testing;
    using BCrypt.Net;

    public class DatabaseTests
    {
        public void ShouldPersist()
        {
            var id = Guid.NewGuid();
            var employee = new Employee
            {
                Id = id,
                Email = SampleEmail(),
                FirstName = "First",
                LastName = "Last",
                JobTitle = "Title",
                Office = Office.Monterrey,
                PhoneNumber = "555-123-4567",
                HashedPassword = BCrypt.HashPassword("password")
            };
            Transaction(database =>
            {
                database.Employee.Add(employee);
            });
            var loaded = Query<Employee>(id);
            loaded.ShouldMatch(employee);
        }

        public void EmployeeRoleShouldPersist()
        {
            var id = Guid.NewGuid();
            var employee = new Employee
            {
                Id = id,
                Email = SampleEmail(),
                FirstName = "First",
                LastName = "Last",
                JobTitle = "Title",
                Office = Office.Monterrey,
                PhoneNumber = "555-123-4567",
                HashedPassword = BCrypt.HashPassword("password")
            };
            var roleId = Guid.NewGuid();
            var role = new Role
            {
                Id = roleId,
                Name = "Manager",
                RolePermission = new List<RolePermission>()
            };
            var employeeRoleId = Guid.NewGuid();
            var employeeRoles = new EmployeeRoles
            {
                Employee = employee,
                Role = role,
                Id = employeeRoleId
            };
            Transaction(database =>
            {
                database.EmployeeRoles.Add(employeeRoles);
            });
            
            Transaction(database => { database.EmployeeRoles.
                Where(x => x.Id == employeeRoleId).ShouldMatch(employeeRoles); });
        }

        public void RoleShouldPersist()
        {
            var roleId = Guid.NewGuid();
            var role = new Role
            {
                Id = roleId,
                Name = "Manager",
                RolePermission = new List<RolePermission>()
            };
            Transaction(database =>
            {
                database.Role.Add(role);
            });

            Transaction(database => {
                database.Role.
                    Where(x => x.Id == roleId).ShouldMatch(role);
            });
        }

        public void RolePermissionShouldPersist()
        {
            var roleId = Guid.NewGuid();
            var role = new Role
            {
                Id = roleId,
                Name = "Manager",
                RolePermission = new List<RolePermission>()
            };
            var permission = Permission.Edit;
            var rolePermissionId = Guid.NewGuid();
            var rolePermission = new RolePermission
            {
                Id = rolePermissionId,
                Permission = permission,
                Role = role
            };
            Transaction(database =>
            {
                database.RolePermission.Add(rolePermission);
            });

            Transaction(database => {
                database.RolePermission.
                    Where(x => x.Id == rolePermissionId).ShouldMatch(rolePermission);
            });
        }
    }
}
