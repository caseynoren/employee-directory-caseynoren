﻿namespace EmployeeDirectory.Tests.Features.Account
{
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Account;
    using EmployeeDirectory.Features.Employees;
    using EmployeeDirectory.Model;
    using Should;
    using static Testing;

    class ChangePasswordTests
    {
        public async Task ShouldHaveChangedPasswordSucceedOnLoginAttempt()
        {
            var employee = await LogIn();

            await Send(new ChangePassword.Command()
            {
                CurrentPassword = "password",
                NewPassword = "newPassword",
                ConfirmPassword = "newPassword"
            });

            new LogIn.Command()
            {
                Email = employee.Email,
                Password = "newPassword"
            }.ShouldValidate();
        }

        public async Task ShouldThrowErrorWhenCurrentPasswordIsIncorrect()
        {
            await LogIn();

            new ChangePassword.Command()
            {
                CurrentPassword = "wrongpassword!",
                NewPassword = "newPassword",
                ConfirmPassword = "newPassword"
            }.ShouldNotValidate("Incorrect Password");
        }

        public async Task ShouldThrowAllErrorsWhenFormIsBlank()
        {
            await LogIn();

            new ChangePassword.Command().ShouldNotValidate(
                "'Confirm Password' must not be empty.",
                "'Current Password' must not be empty.",
                "Incorrect Password",
                "'New Password' must not be empty.");
        }
    }
}
