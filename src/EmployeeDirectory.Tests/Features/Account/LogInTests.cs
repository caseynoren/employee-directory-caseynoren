﻿namespace EmployeeDirectory.Tests.Features.Account
{
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Account;
    using EmployeeDirectory.Features.Employees;
    using EmployeeDirectory.Model;
    using static EmployeeDirectory.Tests.Testing;

    class LogInTests
    {
        public void ShouldThrowErrorWhenFormIsEmpty()
        {
            var validator = new LogIn.Command();
            validator.ShouldNotValidate(
                "The specified condition was not met for 'Email'.",
                "The specified condition was not met for 'Password'.");
        }

        public async Task ShouldThrowErrorWhenPasswordIsWrong()
        {
            var email = SampleEmail();
            await Send(new AddEmployee.Command
            {
                FirstName = "Gomez",
                LastName = "Adams",
                Email = email,
                JobTitle = "Office Worker",
                Office = Office.Austin,
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            });

            var login = new LogIn.Command
            {
                Email = email,
                Password = "ilovenobody"
            };

            login.ShouldNotValidate("The specified condition was not met for 'Email'.",
            "The specified condition was not met for 'Password'.");
        }

        public async Task ShouldThrowErrorWhenUsernameIsWrong()
        {
            var email = SampleEmail();
            var wrongEmail = SampleEmail();
            await Send(new AddEmployee.Command
            {
                FirstName = "Gomez",
                LastName = "Adams",
                Email = email,
                JobTitle = "Office Worker",
                Office = Office.Austin,
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            });

            var login = new LogIn.Command
            {
                Email = wrongEmail,
                Password = "ilovemorticia"
            };

            login.ShouldNotValidate("The specified condition was not met for 'Email'.",
                "The specified condition was not met for 'Password'.");
        }

        public async Task ShouldLogInUserWithCorrectCredentials()
        {
            var email = SampleEmail();
            await Send(new AddEmployee.Command
            {
                FirstName = "Gomez",
                LastName = "Adams",
                Email = email,
                JobTitle = "Office Worker",
                Office = Office.Austin,
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            });

            var login = new LogIn.Command
            {
                Email = email,
                Password = "ilovemorticia"
            };

            login.ShouldValidate();
        }
    }
}
