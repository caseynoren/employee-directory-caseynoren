﻿using static EmployeeDirectory.Tests.Testing;

namespace EmployeeDirectory.Tests.Features.Employees
{
    using System;
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Employees;
    using EmployeeDirectory.Model;

    class AddEmployeeTests
    {
        public async Task ShouldAddRequiredPropertiesToNewEmployee()
        {
            var response = await Send(new AddEmployee.Command
            {
                FirstName = "Gomez",
                LastName = "Adams",
                Email = SampleEmail(),
                JobTitle = "Office Worker",
                Office = Office.Austin,
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            });

            var added = Query<Employee>(response.EmployeeId);
            added.ShouldMatch(
                new Employee
                {
                    FirstName = "Gomez",
                    LastName = "Adams",
                    Email = added.Email,
                    JobTitle = "Office Worker",
                    Office = Office.Austin,
                    Id = response.EmployeeId,
                    HashedPassword = added.HashedPassword
                });
        }

        public void ShouldThrowErrorWhenFieldsAreEmpty()
        {
            var validator = new AddEmployee.Command();
            validator.ShouldNotValidate(
                "'Email' must not be empty.",
                "'First Name' must not be empty.",
                "'Last Name' must not be empty.",
                "'Job Title' must not be empty.",
                "'Office' must not be empty.",
                "'Password' must not be empty.",
                "'Confirm Password' must not be empty.");
        }

        public void ShouldThrowErrorWithInvalidEmail()
        {
            var validator = new AddEmployee.Command
            {
                Email = Guid.NewGuid().ToString(),
                FirstName = "Casey",
                JobTitle = "Engineering Intern",
                LastName = "Koster",
                Office = Office.Austin,
                PhoneNumber = "333-333-3333",
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            };
            validator.ShouldNotValidate("'Email' is not a valid email address.");
        }

        public void ShouldThrowErrorWhenFirstNameIsTooLong()
        {
            var validator = new AddEmployee.Command
            {
                Email = SampleEmail(),
                FirstName = new string('c', 256),
                JobTitle = "Engineering Intern",
                LastName = "Koster",
                Office = Office.Austin,
                PhoneNumber = "333-333-3333",
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            };
            validator.ShouldNotValidate("The length of 'First Name' must be 255 characters or fewer. You entered 256 characters.");
        }

        public async Task ShouldThrowErrorIfEmailIsDuplicatedInDatabase()
        {
            var email = SampleEmail();
            var response = await Send(new AddEmployee.Command
            {
                FirstName = "Gomez",
                LastName = "Adams",
                Email = email,
                JobTitle = "Office Worker",
                Office = Office.Austin,
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            });

            var added = Query<Employee>(response.EmployeeId);

            var validator = new AddEmployee.Command
            {
                FirstName = added.FirstName,
                LastName = added.LastName,
                Email = added.Email,
                JobTitle = added.JobTitle,
                Office = added.Office,
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            };
            validator.ShouldNotValidate("Email address is not available.");
        }
    }
}
