﻿namespace EmployeeDirectory.Tests.Features.Employees
{
    using System;
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Employees;
    using EmployeeDirectory.Model;
    using static Testing;

    class EditEmployeeTests
    {
        private readonly Employee _gomez = new Employee { Id = Guid.NewGuid(), FirstName = "Gomez", LastName = "Adams", JobTitle = "Dad", Office = Office.Austin, Email = SampleEmail(), PhoneNumber = "999-999-9999", HashedPassword = "ilovemorticia"};
        private readonly Employee _morticia = new Employee { Id = Guid.NewGuid(), FirstName = "Morticia", LastName = "Adams", Office = Office.Austin, Email = SampleEmail(), JobTitle = "Mom", PhoneNumber = "666-666-6666", HashedPassword = "ilovegomez"};
        private readonly Employee _curious = new Employee { Id = Guid.NewGuid(), FirstName = "Curious", LastName = "George", JobTitle = "Monkey", Office = Office.Houston, Email = SampleEmail(), PhoneNumber = "333-333-3333", HashedPassword = "ilovethemanintheyellowhat"};

        public async Task ShouldUpdateEveryPropertyOfEmployee()
        {
            Transaction(database =>
            {
                database.Employee.Add(_gomez);
            });
            Transaction(database =>
            {
                database.Employee.Add(_morticia);
            });
            Transaction(database =>
            {
                database.Employee.Add(_curious);
            });

            var gomezId = _gomez.Id;

            var gomezForm = await Send(new EditEmployee.Query()
            {
                Id = gomezId
            });

            gomezForm.Email = SampleEmail();
            gomezForm.FirstName = "Casey";
            gomezForm.LastName = "Koster";
            gomezForm.JobTitle = "Engineering Intern";
            gomezForm.Office = Office.Austin;
            gomezForm.PhoneNumber = "3219605809";

            await Send(gomezForm);

            var edited = Query<Employee>(gomezId);
            edited.ShouldMatch(new Employee
            {
                Email = edited.Email,
                FirstName = "Casey",
                LastName = "Koster",
                JobTitle = "Engineering Intern",
                Office = Office.Austin,
                PhoneNumber = "3219605809",
                Id = gomezId,
                HashedPassword = _gomez.HashedPassword
            });
        }

        public void ShouldThrowErrorWhenFieldsAreEmpty()
        {
            var validator = new EditEmployee.Command();
            validator.ShouldNotValidate(
                "'Email' must not be empty.",
                "'First Name' must not be empty.",
                "'Last Name' must not be empty.",
                "'Job Title' must not be empty.",
                "'Office' must not be empty.");
        }

        public void ShouldThrowErrorWithInvalidEmail()
        {
            var validator = new EditEmployee.Command
            {
                Email = Guid.NewGuid().ToString(),
                FirstName = "Casey",
                JobTitle = "Engineering Intern",
                LastName = "Koster",
                Office = Office.Austin,
                PhoneNumber = "333-333-3333"
            };
            validator.ShouldNotValidate("'Email' is not a valid email address.");
        }

        public void ShouldThrowErrorWhenFirstNameIsTooLong()
        {
            var validator = new EditEmployee.Command
            {
                Email = SampleEmail(),
                FirstName = new string('c', 256),
                JobTitle = "Engineering Intern",
                LastName = "Koster",
                Office = Office.Austin,
                PhoneNumber = "333-333-3333"
            };
            validator.ShouldNotValidate("The length of 'First Name' must be 255 characters or fewer. You entered 256 characters.");
        }

        public async Task ShouldNotThrowErrorIfEmailIsDuplicatedInDatabaseButItIsYourOwn()
        {
            var email = SampleEmail();
            var employee = await Send(new AddEmployee.Command
            {
                FirstName = "Gomez",
                LastName = "Adams",
                Email = email,
                JobTitle = "Office Worker",
                Office = Office.Austin,
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            });

            var validate = await Send(new EditEmployee.Query()
            {
                Id = employee.EmployeeId
            });

            validate.Email = email;
            validate.FirstName = "Casey";
            validate.LastName = "Koster";
            validate.JobTitle = "Engineering Intern";
            validate.Office = Office.Austin;
            validate.PhoneNumber = "3219605809";

            await Send(validate);

            validate.ShouldValidate();
        }

        public async Task ShouldThrowErrorIfEmailIsDuplicatedInDatabase()
        {
            var email = SampleEmail();
            var emailTwo = SampleEmail();
            var employee = await Send(new AddEmployee.Command
            {
                FirstName = "Gomez",
                LastName = "Adams",
                Email = email,
                JobTitle = "Office Worker",
                Office = Office.Austin,
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            });

            await Send(new AddEmployee.Command
            {
                Email = emailTwo,
                FirstName = "Casey",
                JobTitle = "Engineering Intern",
                LastName = "Koster",
                Office = Office.Austin,
                PhoneNumber = "333-333-3333",
                Password = "ilovemorticia",
                ConfirmPassword = "ilovemorticia"
            });

            var validate = await Send(new EditEmployee.Query()
            {
                Id = employee.EmployeeId
            });

            validate.Email = emailTwo;
            validate.FirstName = "Gomez";
            validate.LastName = "Adams";
            validate.JobTitle = "Office Worker";
            validate.Office = Office.Houston;

            validate.ShouldNotValidate("Email address is not available.");
        }
    }
}
