﻿using EmployeeDirectory.Features.Employees;
using EmployeeDirectory.Model;
using System;
using System.Threading.Tasks;
using static EmployeeDirectory.Tests.Testing;
using BCrypt.Net;

namespace EmployeeDirectory.Tests.Features.Employees
{
    using Should;

    class DeleteEmployeeTests
    {
        private readonly Employee _gomez = new Employee { Id = Guid.NewGuid(), FirstName = "Gomez", LastName = "Adams", JobTitle = "Dad", Office = Office.Austin, Email = SampleEmail(), PhoneNumber = "999-999-9999", HashedPassword = "ilovemorticia"};
        private readonly Employee _morticia = new Employee { Id = Guid.NewGuid(), FirstName = "Morticia", LastName = "Adams", Office = Office.Austin, Email = SampleEmail(), JobTitle = "Mom", PhoneNumber = "666-666-6666", HashedPassword = "ilovegomez"};

        public async Task ShouldRemoveEmployeeFromDatabase()
        {
            Transaction(database =>
            {
                database.Employee.Add(_gomez);
            });
            Transaction(database =>
            {
                database.Employee.Add(_morticia);
            });

            await Send(new DeleteEmployee.Command()
            {
                Id = _gomez.Id
            });

            var deletedGomez = Query<Employee>(_gomez.Id);
            var nonDeletedMorticia = Query<Employee>(_morticia.Id);

            deletedGomez.ShouldBeNull();
            nonDeletedMorticia.LastName.ShouldEqual("Adams");
        }

        public async Task ShouldRemoveRoleFromDatabase()
        {
            Transaction(database =>
            {
                database.Employee.Add(_gomez);
            });

            await Send(new DeleteEmployee.Command()
            {
                Id = _gomez.Id
            });

            var deletedGomez = Query<EmployeeRoles>(_gomez.Id);

            deletedGomez.ShouldBeNull();
        }
    }
}
