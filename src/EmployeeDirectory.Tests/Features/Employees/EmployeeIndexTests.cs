﻿namespace EmployeeDirectory.Tests.Features.Employees
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using EmployeeDirectory.Features.Employees;
    using EmployeeDirectory.Model;
    using Should;
    using static Testing;

    class EmployeeIndexTests
    {
        private readonly Employee _gomez = new Employee { Id = Guid.NewGuid(), FirstName = "Gomez", LastName = "Adams", JobTitle = "Dad", Office = Office.Austin, Email = SampleEmail(), PhoneNumber = "999-999-9999", HashedPassword = "ilovemorticia"};
        private readonly Employee _morticia = new Employee { Id = Guid.NewGuid(), FirstName = "Morticia", LastName = "Adams", Office = Office.Austin, Email = SampleEmail(), JobTitle = "Mom", PhoneNumber = "666-666-6666", HashedPassword = "ilovegomez"};
        private readonly Employee _curious = new Employee { Id = Guid.NewGuid(), FirstName = "Curious", LastName = "George", JobTitle = "Monkey", Office = Office.Houston, Email = SampleEmail(), PhoneNumber = "333-333-3333", HashedPassword = "ilovethemanintheyellowhat"};

        public async Task ShouldDisplayLastNameFirst()
        {
            Transaction(database =>
            {
                database.Employee.Add(_gomez);
            });
            Transaction(database =>
            {
                database.Employee.Add(_morticia);
            });
            Transaction(database =>
            {
                database.Employee.Add(_curious);
            });

            var expectedIds = new[] { _gomez.Id, _morticia.Id, _curious.Id };

            var query = new EmployeeIndex.Query();

            var result = await Send(query);
            
            result.Length.ShouldEqual(Count<Employee>());

            result = result.Where(x => expectedIds.Contains(x.Id)).ToArray();

            result[0].FullName.ShouldEqual("Adams, Gomez");
            result[1].FullName.ShouldEqual("Adams, Morticia");
            result[2].FullName.ShouldEqual("George, Curious");
        }

        public async Task ShouldBeOrderedByName()
        {
            Transaction(database =>
            {
                database.Employee.Add(_gomez);
            });
            Transaction(database =>
            {
                database.Employee.Add(_morticia);
            });
            Transaction(database =>
            {
                database.Employee.Add(_curious);
            });

            var expectedIds = new[] { _gomez.Id, _morticia.Id, _curious.Id };

            var query = new EmployeeIndex.Query();

            var result = await Send(query);

            result.Length.ShouldEqual(Count<Employee>());

            result.Where(x => expectedIds.Contains(x.Id)).ShouldMatch(
                new EmployeeIndex.ViewModel { Id = _gomez.Id, FirstName = "Gomez", LastName = "Adams", JobTitle = "Dad", Office = Office.Austin, Email = _gomez.Email, PhoneNumber = "999-999-9999" },
                new EmployeeIndex.ViewModel { Id = _morticia.Id, FirstName = "Morticia", LastName = "Adams", Office = Office.Austin, Email = _morticia.Email, JobTitle = "Mom", PhoneNumber = "666-666-6666" },
                new EmployeeIndex.ViewModel { Id = _curious.Id, FirstName = "Curious", LastName = "George", JobTitle = "Monkey", Office = Office.Houston, Email = _curious.Email, PhoneNumber = "333-333-3333" });
        }

        public async Task ShouldRequireAllPropertiesNamedInViewModel()
        {
            Transaction(database =>
            {
                database.Employee.Add(_morticia);
            });

            var expectedId = _morticia.Id;

            var query = new EmployeeIndex.Query();

            var result = await Send(query);

            result.Length.ShouldEqual(Count<Employee>());

            result.Where(x => x.Id == expectedId).ShouldMatch(
                new EmployeeIndex.ViewModel
                {
                    Id = _morticia.Id,
                    FirstName = "Morticia",
                    LastName = "Adams",
                    Office = Office.Austin,
                    Email = _morticia.Email,
                    JobTitle = "Mom",
                    PhoneNumber = "666-666-6666",
                });
        }
    }
}
