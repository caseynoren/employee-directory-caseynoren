﻿namespace EmployeeDirectory.Model
{
    public enum Permission
    {
        Add = 1,
        Delete = 2,
        Edit = 3,
        ManageSecurity = 4
    }
}
