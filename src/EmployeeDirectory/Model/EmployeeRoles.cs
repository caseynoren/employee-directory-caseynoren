﻿namespace EmployeeDirectory.Model
{
    public class EmployeeRoles : Entity
    {
        public virtual Employee Employee { get; set; }
        public virtual Role Role { get; set; }
    }
}
