﻿namespace EmployeeDirectory.Model
{
    using System.Collections.Generic;

    public class Role : Entity
    {
        public string Name { get; set; }
        public virtual ICollection<RolePermission> RolePermission { get; set; }
    }
}
