﻿namespace EmployeeDirectory.Model
{
    public class UserContext
    {
        public Employee User { get; set; }
        public bool IsAuthenticated => User != null;
    }
}
