﻿namespace EmployeeDirectory.Features.Account
{
    using System.Threading.Tasks;
    using Infrastructure;
    using MediatR;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Model;

    public class AccountController : Controller
    {
        private readonly IMediator _mediator;
        private readonly ILoginService _loginService;
        private readonly UserContext _userContext;

        public AccountController(IMediator mediator, ILoginService loginService, UserContext userContext)
        {
            _mediator = mediator;
            _loginService = loginService;
            _userContext = userContext;
        }

        [AllowAnonymous]
        public ActionResult LogIn()
        {
            if (_userContext.IsAuthenticated)
            {
                return RedirectToAction("Index", "Employees");
            }
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> LogIn(LogIn.Command command)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(command);
                return RedirectToAction("Index", "Employees");
            }

            TempData["Message"] = "Log in failed";
            TempData["Type"] = "error";
            return View();
        }

        public async Task<ActionResult> LogOut()
        {
            await _loginService.LogOut();
            return RedirectToAction("LogIn", "Account");
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(ChangePassword.Command command)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(command);
                return RedirectToAction("Index", "Employees");
            }

            return View();
        }
    }
}
