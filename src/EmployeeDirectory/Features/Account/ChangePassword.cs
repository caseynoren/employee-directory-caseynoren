﻿namespace EmployeeDirectory.Features.Account
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using MediatR;
    using Model;
    using BCrypt.Net;
    using FluentValidation;
    using Infrastructure;

    public class ChangePassword
    {
        public class Command : IRequest<Response>
        {
            [Display(Name = "Current Password")]
            [DataType(DataType.Password)]
            public string CurrentPassword { get; set; }

            [Display(Name = "New Password")]
            [DataType(DataType.Password)]
            public string NewPassword { get; set; }

            [Display(Name = "Confirm Password")]
            [DataType(DataType.Password)]
            public string ConfirmPassword { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            private readonly UserContext _userContext;

            public Validator(UserContext userContext)
            {
                _userContext = userContext;

                RuleFor(employee => employee.CurrentPassword).NotNull().Must(CurrentPasswordCorrect).WithMessage("Incorrect Password");
                RuleFor(employee => employee.NewPassword).NotNull().Equal(customer => customer.ConfirmPassword).WithMessage("Passwords do not match.");
                RuleFor(employee => employee.ConfirmPassword).NotNull().Equal(customer => customer.NewPassword).WithMessage("Passwords do not match.");
            }

            private bool CurrentPasswordCorrect(Command form, string currentPassword)
            {
                var employee = _userContext.User;

                if (form.CurrentPassword == null || employee == null)
                {
                    return false;
                }

                var password = BCrypt.Verify(form.CurrentPassword, employee.HashedPassword);
                if (password)
                {
                    return true;
                }

                return false;
            }
        }

        public class Response
        {
            public Employee Employee { get; set; }
        }

        public class QueryHandler : RequestHandler<Command, Response>
        {
            private readonly UserContext _userContext;
            private readonly DirectoryContext _context;
            private readonly ILoginService _loginService;

            public QueryHandler(UserContext userContext, DirectoryContext context, ILoginService loginService)
            {
                _userContext = userContext;
                _context = context;
                _loginService = loginService;
            }

            protected override Response HandleCore(Command message)
            {
                var employee = _userContext.User;
                var newPassword = BCrypt.HashPassword(message.ConfirmPassword);
                employee.HashedPassword = newPassword;

                _context.Employee.Update(employee);

                return new Response
                {
                    Employee = employee
                };
            }
        }
    }
}

