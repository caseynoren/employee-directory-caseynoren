﻿namespace EmployeeDirectory.Features.Account
{
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using MediatR;
    using Model;
    using BCrypt.Net;
    using FluentValidation;
    using Infrastructure;

    public class LogIn
    {
        public class Command : IRequest<Response>
        {
            public string Email { get; set; }

            [DataType(DataType.Password)]
            public string Password { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            private readonly DirectoryContext _context;
            public Validator(DirectoryContext dbcontext)
            {
                _context = dbcontext;

                RuleFor(employee => employee.Email).EmailAddress().MaximumLength(255).Must(PasswordAndEmailMatch);
                RuleFor(employee => employee.Password).MaximumLength(255).Must(PasswordAndEmailMatch);
            }

            private bool PasswordAndEmailMatch(Command form, string email)
            {
                var employee =
                    _context.Employee
                        .SingleOrDefault(x => x.Email == form.Email);

                if (form.Password == null || email == null || employee == null)
                {
                    return false;
                }

                var password = BCrypt.Verify(form.Password, employee.HashedPassword);
                if (password)
                {
                    return true;
                }

                return false;
            }
        }

        public class Response
        {
            public Employee Employee { get; set; }
        }

        public class QueryHandler : RequestHandler<Command, Response>
        {
            private readonly DirectoryContext _context;
            private readonly ILoginService _loginService;

            public QueryHandler(DirectoryContext context, ILoginService loginService)
            {
                _context = context;
                _loginService = loginService;
            }

            protected override Response HandleCore(Command message)
            {
                var employee =
                    _context.Employee
                        .Single(x => x.Email == message.Email);
                _loginService.LogIn(employee.Email);

                return new Response
                {
                    Employee = employee
                };
            }
        }
    }
}
