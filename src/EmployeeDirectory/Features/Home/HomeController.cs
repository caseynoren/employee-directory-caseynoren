﻿namespace EmployeeDirectory.Features.Home
{
    using Microsoft.AspNetCore.Mvc;

    public class HomeController : Controller
    {
        public IActionResult Index() => RedirectToAction("LogIn", "Account");

        public IActionResult Error() => View();
    }
}
