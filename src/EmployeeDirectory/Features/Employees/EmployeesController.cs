﻿namespace EmployeeDirectory.Features.Employees
{
    using System.Threading.Tasks;
    using Infrastructure;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using Model;

    public class EmployeesController : Controller
    {
        private readonly IMediator _mediator;
        private readonly ILoginService _loginService;
        private readonly UserContext _userContext;

        public EmployeesController(IMediator mediator, ILoginService loginService, UserContext userContext)
        {
            _mediator = mediator;
            _loginService = loginService;
            _userContext = userContext;
        }

        public async Task<ActionResult> Index(EmployeeIndex.Query query)
        {
            return View(await _mediator.Send(query));
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(AddEmployee.Command command)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(command);
                return RedirectToAction("Index");
            }

            return View();
        }

        public async Task<ActionResult> Edit(EditEmployee.Query query)
        {
            return View(await _mediator.Send(query));
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditEmployee.Command command)
        {
            if (ModelState.IsValid)
            {
                await _mediator.Send(command);
                await _loginService.LogOut();
                await _loginService.LogIn(command.Email);
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteEmployee.Command command)
        {
            if (ModelState.IsValid)
            {
                if (command.Id == _userContext.User.Id)
                {
                    TempData["Message"] = "Can't delete yo'self";
                    TempData["Type"] = "error";
                    return Json(new { redirectUrl = Url.Action("Index", "Employees") });
                }
                var employee = await _mediator.Send(command);

                TempData["Message"] = "You deleted " + employee.FirstName + " " + employee.LastName;
                TempData["Type"] = "success";

                return Json(new { redirectUrl = Url.Action("Index", "Employees")});
            }

            TempData["Message"] = "Can't delete system admin";
            TempData["Type"] = "error";
            return Json(new { redirectUrl = Url.Action("Index", "Employees") });
        }
    }
}
