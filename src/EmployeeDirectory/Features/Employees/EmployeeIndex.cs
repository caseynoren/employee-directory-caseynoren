﻿using System.Linq;

namespace EmployeeDirectory.Features.Employees
{
    using System;
    using AutoMapper;
    using MediatR;
    using Model;

    public class EmployeeIndex
    {
        public class Query : IRequest<ViewModel[]> { }

        public class ViewModel
        {
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string JobTitle { get; set; }
            public Office Office { get; set; }
            public string PhoneNumber { get; set; }
            public string FullName => LastName + ", " + FirstName;
            public Guid Id { get; set; }
        }

        public class QueryHandler : RequestHandler<Query, ViewModel[]>
        {
            private readonly DirectoryContext _context;
            private readonly IMapper _mapper;

            public QueryHandler(DirectoryContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            protected override ViewModel[] HandleCore(Query request)
            {
                var model = _context.Employee
                    .OrderBy(x => x.LastName)
                    .ThenBy(x => x.FirstName)
                    .ToArray();

                var mappedModel = _mapper.Map<ViewModel[]>(model);

                return mappedModel;
            }
        }
    }
}
