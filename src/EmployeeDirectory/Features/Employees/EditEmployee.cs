﻿using System;
using System.Linq;

namespace EmployeeDirectory.Features.Employees
{
    using System.ComponentModel.DataAnnotations;
    using AutoMapper;
    using FluentValidation;
    using MediatR;
    using Model;

    public class EditEmployee
    {
        public class Query : IRequest<Command>
        {
            public Guid Id { get; set;  }
        }

        public class Command : IRequest
        {
            [Required]
            public Guid Id { get; set; }

            [Required]
            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            [Required]
            public string Email { get; set; }

            [Display(Name = "Phone Number")]
            public string PhoneNumber { get; set; }

            [Required]
            [Display(Name = "Job Title")]
            public string JobTitle { get; set; }

            [Required]
            [Display(Name = "Office")]
            public Office? Office { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            private readonly DirectoryContext _context;
            public Validator(DirectoryContext dbcontext)
            {
                _context = dbcontext;

                RuleFor(employee => employee.Email).EmailAddress();
                RuleFor(customer => customer.FirstName).NotNull();
                RuleFor(customer => customer.Email).NotNull();
                RuleFor(customer => customer.JobTitle).NotNull();
                RuleFor(customer => customer.LastName).NotNull();
                RuleFor(customer => customer.Office).NotNull();
                RuleFor(employee => employee.FirstName).MaximumLength(255);
                RuleFor(employee => employee.Email).MaximumLength(255);
                RuleFor(employee => employee.JobTitle).MaximumLength(255);
                RuleFor(employee => employee.LastName).MaximumLength(255);
                RuleFor(employee => employee.PhoneNumber).MaximumLength(20);
                RuleFor(x => x.Email).Must(BeAUniqueEmailUnlessItsYourOwn).WithMessage("Email address is not available.");
            }

            private bool BeAUniqueEmailUnlessItsYourOwn(Command form, string email)
            {
                var db = _context.Employee
                    .SingleOrDefault(x => x.Email == email);

                if (db == null)
                    return true;

                return db.Id == form.Id;
            }
        }

        public class QueryHandler : RequestHandler<Query, Command>
        {
            private readonly DirectoryContext _context;
            private readonly IMapper _mapper;

            public QueryHandler(DirectoryContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            protected override Command HandleCore(Query query)
            {
                var employee = _context.Employee.Single(x => x.Id == query.Id);

                var model = _mapper.Map<Command>(employee);

                return model;
            }
        }

        public class CommandHandler : RequestHandler<Command>
        {
            private readonly DirectoryContext _context;
            private readonly IMapper _mapper;

            public CommandHandler(DirectoryContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            protected override void HandleCore(Command form)
            {

                var employee = _context.Employee.Single(x => x.Id == form.Id);
                _mapper.Map(form, employee);
            }
        }
    }
}
