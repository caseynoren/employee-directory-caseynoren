﻿using AutoMapper;

namespace EmployeeDirectory.Features.Employees
{
    using Model;

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AddEmployee.Command, Employee>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.HashedPassword, opt => opt.Ignore());

            CreateMap<EditEmployee.Command, Employee>()
                .ForMember(x => x.HashedPassword, opt => opt.Ignore());
            CreateMap<Employee, EditEmployee.Command>();

            CreateMap<Employee, EmployeeIndex.ViewModel>();
        }
    }
}
