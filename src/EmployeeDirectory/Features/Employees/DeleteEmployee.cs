﻿namespace EmployeeDirectory.Features.Employees
{
    using System;
    using System.Linq;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using Model;

    public class DeleteEmployee
    {
        public class Command : IRequest<Response>
        {
            public Guid Id { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            private readonly DirectoryContext _context;
            public Validator(DirectoryContext dbcontext)
            {
                _context = dbcontext;

                RuleFor(employee => employee).Must(SystemAdminCantBeDeleted);
            }

            private bool SystemAdminCantBeDeleted(Command form)
            {
                var rolePermissions = _context.EmployeeRoles.Where(x => x.Employee.Id == form.Id)
                    .Include(x => x.Employee)
                    .Include(x => x.Role)
                    .Where(r => r.Role.RolePermission.Any(p => p.Permission == Permission.ManageSecurity)).ToList();

                if (rolePermissions.Count != 0)
                {
                    return false;
                }

                return true;
            }
        }

        public class Response
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }

        public class CommandHandler : RequestHandler<Command, Response>
        {
            private readonly DirectoryContext _context;

            public CommandHandler(DirectoryContext context)
            {
                _context = context;
            }
            protected override Response HandleCore(Command message)
            {
                var employee = _context.Employee.Single(x => x.Id == message.Id);
                var employeeRoles = _context.EmployeeRoles.Where(x => x.Employee == employee).ToList();

                foreach (var role in employeeRoles)
                {
                    _context.EmployeeRoles.Remove(role);
                }
                _context.Employee.Remove(employee);

                return new Response
                {
                    FirstName = employee.FirstName,
                    LastName = employee.LastName
                };
            }
        }
    }
}

