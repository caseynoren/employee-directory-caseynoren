﻿using System;

namespace EmployeeDirectory.Features.Employees
{
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using AutoMapper;
    using FluentValidation;
    using MediatR;
    using Model;
    using BCrypt.Net;

    public class AddEmployee
    {
        public class Command : IRequest<Response>
        {
            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            public string Email { get; set; }

            [Display(Name = "Phone Number")]
            public string PhoneNumber { get; set; }

            [Display(Name = "Job Title")]
            public string JobTitle { get; set; }

            public Office? Office { get; set; }

            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Confirm Password")]
            [DataType(DataType.Password)]
            public string ConfirmPassword { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {
            private readonly DirectoryContext _context;
            public Validator(DirectoryContext dbcontext)
            {
                _context = dbcontext;

                RuleFor(employee => employee.Email).EmailAddress().MaximumLength(255);
                RuleFor(customer => customer.FirstName).NotNull();
                RuleFor(customer => customer.Email).NotNull();
                RuleFor(customer => customer.JobTitle).NotNull();
                RuleFor(customer => customer.LastName).NotNull();
                RuleFor(customer => customer.Office).NotNull();
                RuleFor(customer => customer.Password).NotNull();
                RuleFor(customer => customer.ConfirmPassword).NotNull();
                RuleFor(employee => employee.FirstName).MaximumLength(255);
                RuleFor(employee => employee.JobTitle).MaximumLength(255);
                RuleFor(employee => employee.LastName).MaximumLength(255);
                RuleFor(employee => employee.PhoneNumber).MaximumLength(20);
                RuleFor(customer => customer.ConfirmPassword).Equal(customer => customer.Password).WithMessage("Passwords do not match.");
                RuleFor(x => x.Email).Custom((email, context) => {
                    if (_context.Employee.SingleOrDefault(x => x.Email == email) != null)
                    {
                        context.AddFailure("Email address is not available.");
                    }
                });
            }
        }

        public class Response
        {
            public Guid EmployeeId { get; set; }
        }

        public class QueryHandler : RequestHandler<Command, Response>
        {
            private readonly DirectoryContext _context;
            private readonly IMapper _mapper;

            public QueryHandler(DirectoryContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            protected override Response HandleCore(Command message)
            {
                var employee = _mapper.Map<Employee>(message);
                employee.Id = Guid.NewGuid();
                employee.HashedPassword = BCrypt.HashPassword(message.Password);

                _context.Employee.Add(employee);

                return new Response
                {
                    EmployeeId = employee.Id
                };
            }
        }
    }
}
