﻿namespace EmployeeDirectory.Infrastructure
{
    using System;
    using System.Linq.Expressions;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;

    public static class HtmlHelperExtensions
    {
        public static TagBuilder Required<TModel>(this IHtmlHelper<TModel> html)
        {
            var span = new TagBuilder("span");
            span.MergeAttribute("style", "color:red;");
            span.InnerHtml.Append("*");

            return span;
        }

        public static TagBuilder Input<TModel, TValue>(this IHtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            var htmlAttributes = new { @class = "form-control" };

            var div = new TagBuilder("div");
            div.InnerHtml.AppendHtml(html.LabelFor(expression));
            var isRequired = IsRequired(html, expression);
            if (isRequired)
            {
                div.InnerHtml.AppendHtml(Required(html));
            }

            var isEnum = IsEnumType(typeof(TValue));
            if (isEnum)
            {
                div.InnerHtml.AppendHtml(html.DropDownListFor(expression, html.GetEnumSelectList(typeof(TValue)),
                    "--Select--", htmlAttributes));
            }
            else
            {
                div.InnerHtml.AppendHtml(html.EditorFor(expression, new { htmlAttributes }));
            }

            div.AddCssClass("form-group");

            div.InnerHtml.AppendHtml(html.ValidationMessageFor(expression));

            return div;
        }

        public static TagBuilder SecondaryButton<TModel>(this IHtmlHelper<TModel> html, string text, string url)
        {
            var button = new TagBuilder("a");
            button.MergeAttribute("href", url);
            button.MergeAttribute("role", "button");
            button.AddCssClass("btn");
            button.AddCssClass("btn-secondary");
            button.InnerHtml.Append(text);

            return button;
        }

        public static TagBuilder CancelButton<TModel>(this IHtmlHelper<TModel> html, string url)
        {
            return html.SecondaryButton("Cancel", url);
        }

        public static TagBuilder CreateButton<TModel>(this IHtmlHelper<TModel> html, string text)
        {
            var button = new TagBuilder("button");
            button.MergeAttribute("type", "submit");
            button.AddCssClass("btn");
            button.AddCssClass("btn-primary");
            button.InnerHtml.Append(text);

            return button;
        }

        public static TagBuilder SaveButton<TModel>(this IHtmlHelper<TModel> html)
        {
            var button = new TagBuilder("button");
            button.MergeAttribute("type", "submit");
            button.AddCssClass("btn");
            button.AddCssClass("btn-primary");
            button.InnerHtml.Append("Save");

            return button;
        }

        public static TagBuilder PrimaryButton<TModel>(this IHtmlHelper<TModel> html, string text, string url)
        {
            var button = new TagBuilder("a");
            button.MergeAttribute("href", url);
            button.MergeAttribute("role", "button");
            button.AddCssClass("btn");
            button.AddCssClass("btn-primary");
            button.InnerHtml.Append(text);

            return button;
        }

        public static TagBuilder RegisterNewButton<TModel>(this IHtmlHelper<TModel> html, string url)
        {
            return html.PrimaryButton("Register New Employee", url);
        }

        public static TagBuilder Icon<TModel>(this IHtmlHelper<TModel> html, string text)
        {
            var icon = new TagBuilder("i");
            icon.AddCssClass("material-icons");
            icon.InnerHtml.Append(text);

            return icon;
        }

        private static bool IsRequired<TModel, TValue>(this IHtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            var modelExplorer =
                ExpressionMetadataProvider.FromLambdaExpression(expression, html.ViewData, html.MetadataProvider);

            return modelExplorer.Metadata.IsRequired;
        }

        private static bool IsEnumType(this Type type)
            => type.IsEnum || Nullable.GetUnderlyingType(type)?.IsEnum == true;
    }
}