﻿namespace EmployeeDirectory.Infrastructure
{
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Model;
    using MediatR;
    using Microsoft.AspNetCore.Mvc.Filters;

    public class UserContextFilter : IAsyncAuthorizationFilter
    {
        private readonly DirectoryContext _database;
        private readonly UserContext _userContext;
        private readonly ILoginService _loginService;
        private readonly IMediator _mediator;

        public UserContextFilter(
            DirectoryContext database,
            UserContext userContext,
            ILoginService loginService)
        {
            _database = database;
            _userContext = userContext;
            _loginService = loginService;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var email = context.HttpContext.User.Identity.Name;
            var user = _database.Employee.SingleOrDefault(x => x.Email == email);

            if (email != null && user == null)
            {
                await _loginService.LogOut();
                context.Result = new UnauthorizedResult();
            }
            else
            {
                _userContext.User = user;
            }
        }
    }
}
