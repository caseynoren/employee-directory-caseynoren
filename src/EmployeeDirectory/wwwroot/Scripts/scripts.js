﻿$(document).ready(function() {
    setRequired();
    getMaxLength();
    IncludeAntiForgeryTokenInAjaxPosts();
    var toastrMessage = $('#toastr-message').attr('data-message');
    var toastrType = $('#toastr-message').attr('data-type');
    if (toastrMessage !== "" && toastrType !== "") {
        toastr[toastrType](toastrMessage);
    }
});

$('.input-validation-error').each(function () {
    $('.input-validation-error').addClass('is-invalid');
});
$('.field-validation-error').each(function () {
    $('.field-validation-error').addClass('text-danger');
});

function getMaxLength() {
    $('.form-control').each(function() {
        var maxLengthFromValidation = $(this).attr("data-val-maxlength-max");
        $("input[type='text']").attr('maxlength', maxLengthFromValidation);
    });
}

function setRequired() {
    var $form = $('form');
    $('form').find("[data-val-required]").each(function () {
        var $input = $(this);
        var requiredAsterisk = "<span style=\"color: red\">*</span>";
        var id = $input.attr('id');
        var $label = $form.find("label[for='" + id + "']");
        if ($label.length > 0) {
            var html = $label.html() + "";
            if (html.indexOf(requiredAsterisk) <= 0) $label.html(html + ": " + requiredAsterisk);
        }
    });
};
$('#modal-container').on('show.bs.modal', function(event) {
    var link = $(event.relatedTarget);
    var firstName = link.data('delete-first-name');
    var lastName = link.data('delete-last-name');
    if ($(event.relatedTarget.hasAttribute('delete-first-name'))) {
        document.getElementById("employee-name").innerHTML = "Are you sure you want to delete " + firstName + " " + lastName + "?";
        document.getElementById("modal-label").innerHTML = "Delete";
        document.getElementById("delete-post").innerHTML = "Delete";
    }
});
$(".garbage-icon").on("click",
    function() {
        var id = $(this).data('id');
        $("#delete-post").attr("data-id", id);
    });
$("#delete-post").on("click", function (event) {
    event.preventDefault();
    var id = event.target.dataset.id;
    $.ajax({
            type: "POST",
            url: "Employees/Delete/" + id
        })
        .then(function(data){
            window.location.replace(data.redirectUrl);
        }).catch(function() {
            toastr.error("Something went wrong.");
        });
});

function IncludeAntiForgeryTokenInAjaxPosts() {
    $(document).ajaxSend(function (event, xhr, options) {
        if (options.type.toUpperCase() === "POST") {
            var token = $("#AntiForgeryToken input[name=__RequestVerificationToken]").val();
            xhr.setRequestHeader("RequestVerificationToken", token);
        }
    });
};